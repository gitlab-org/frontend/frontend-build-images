# frontend-build-images

This project houses a few build images, similar to [gitlab-build-images](https://gitlab.com/gitlab-org/gitlab-build-images),
which are of interest for Frontend projects.

## semantic-release

Semantic release, or better `registry.gitlab.com/gitlab-org/frontend/frontend-build-images/semantic-release`
is a docker image which contains the `semantic-release` tool which one can utilize for automatically releasing
new versions of packages to NPM.

The docker image comes with the following plugins:

- `@semantic-release/changelog`: For updating the CHANGELOG within a repository
- `@semantic-release/exec`: For running arbitrary commands
- `@semantic-release/git`: For creating tags and releasing to git
- `@semantic-release/gitlab`: For creating releases within GitLab
- `@semantic-release/npm`: For publishing to NPM

semantic-release is packaged in this docker image, because semantic release has a lot of dependencies (> 1000)
and to make the config a bit more reusable.

### How to integrate semantic-release with your project?

1.  Ensure that your project has the following environment variables `NPM_TOKEN` and `GITLAB_TOKEN`
    (make sure that the user that owns `GITLAB_TOKEN` has sufficient permissions to push to the project's default branch)
1.  Add a config to your project, for example in the `package.json`:

    ```yaml
      "release": {
        "branches": ["main"],
        "verifyConditions": [
          "@semantic-release/changelog",
          "@semantic-release/npm",
          "@semantic-release/git",
          "@semantic-release/gitlab"
        ],
        "prepare": [
          "@semantic-release/changelog",
          "@semantic-release/npm",
          "@semantic-release/git"
        ],
        "publish": [
          "@semantic-release/npm",
          "@semantic-release/gitlab"
        ],
        "success": false,
        "fail": false
      }
    ```

1.  (Recommended) If you are using Merge Request pipelines and/or rules syntax, use the `.gitlab-ci-template.rules.yml`.

    ```yaml
    include:
      - project: 'gitlab-org/frontend/frontend-build-images'
        file: '/semantic-release/.gitlab-ci-template.rules.yml'
    
    # This will auromatically run on the default branch
    semantic-release:
      extends: .semantic-release

    # Run a dry run on Merge Requests
    semantic-release-dry-run:
      extends: .semantic-release
      needs: []
      script: semantic-release --branches $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME --dry-run --no-ci
      rules:
         - if: $CI_MERGE_REQUEST_IID
    ```

    (Legacy) If you are using CI on branches or the only/except syntax, use the `.gitlab-ci-template.yml`.

    ```yaml
    include:
      - project: 'gitlab-org/frontend/frontend-build-images'
        file: '/semantic-release/.gitlab-ci-template.yml'

    # This will auromatically run on the master branch branch
    semantic-release:
      extends: .semantic-release

    semantic-release-dry-run:
      extends: .semantic-release
      needs: []
      script: semantic-release --branches $CI_COMMIT_BRANCH --dry-run --no-ci
      only:
        refs:
          - branches
      except:
        refs:
          - master
    ```

1.  Once you have verified that the semantic release job ran successfully, you can change it to look like this:

    The default semantic-release job runs only on master (or the default branch), if you e.g. want to define a dependency or change the stage,
    you can change the config like this:

    ```yaml
    semantic-release:
      extends: .semantic-release
      needs:
        - build_job
      stage: publish
    ```
