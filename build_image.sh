#!/usr/bin/env sh

set -e

if [ -z "$1" ] || [ ! -d "$1" ]; then
    echo "Please supply an image that you want to build as an argument"
    echo "Valid images are:"
    ls -d -- */
    exit 1
fi

IMAGE_BASE=${CI_REGISTRY_IMAGE:-frontend-build-images}
IMAGE_NAME=$(echo "$1" | sed "s#/##g")
COMMIT_REF=${CI_COMMIT_REF_NAME:-unstable}
IMAGE_TAG=$(cat "$IMAGE_NAME/VERSION")

if [ "$COMMIT_REF" = "main" ]; then
    DOCKER_IMAGE="$IMAGE_BASE/$IMAGE_NAME:$IMAGE_TAG"
else
    IMAGE_POSTFIX=${CI_COMMIT_SHORT_SHA:-unstable}
    DOCKER_IMAGE="$IMAGE_BASE/unstable/$IMAGE_NAME:$IMAGE_TAG-$IMAGE_POSTFIX"
fi

if [ "$CI" = "true" ]; then
    echo "Running on CI, so we check if the image already exists"
    if crane manifest "$DOCKER_IMAGE" >/dev/null; then
        echo "Image $DOCKER_IMAGE already exists"
        exit 0
    fi
fi

echo "Building image $DOCKER_IMAGE"

/kaniko/executor \
  --context "$IMAGE_NAME" \
  --dockerfile "$IMAGE_NAME/Dockerfile" \
  --no-push \
  --destination "$DOCKER_IMAGE" \
  --single-snapshot \
  --tarPath image.tar

if [ "$CI" = "true" ]; then
    echo "Running on CI, so we push the built image"
    crane push image.tar "$DOCKER_IMAGE"
    if [ "$COMMIT_REF" = "main" ]; then
      echo "Tagging $DOCKER_IMAGE as latest"
      crane tag "$DOCKER_IMAGE" latest
    fi
fi
