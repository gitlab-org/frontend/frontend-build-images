#!/bin/sh

if [ -z "$BUILD_IMAGE_NO_MESSAGE" ]; then
    echo "You are using a GitLab CI build-image with kaniko and crane"
    echo "You can suppress this message by setting BUILD_IMAGE_NO_MESSAGE"

    /kaniko/executor version
    echo "Crane version: $(crane version)"
fi

if [ -z "$BUILD_IMAGE_NO_AUTO_LOGIN" ]; then
    if [ -n "$CI_REGISTRY_USER" ] && [ -n "$CI_REGISTRY_PASSWORD" ] && [ -n "$CI_REGISTRY" ]; then
        echo "Logging into GitLab CI registry ($CI_REGISTRY)"
        echo "You can suppress this behavior by setting BUILD_IMAGE_NO_AUTO_LOGIN"
        crane auth login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    fi
fi

if [ -z "$1" ]; then
    /busybox/sh
else
    exec "$@"
fi
