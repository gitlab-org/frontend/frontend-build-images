# BASED OFF OF https://github.com/mhart/alpine-node/blob/02cf3b64f95556370d80e4a9a73ab614b2a15396/build.dockerfile

FROM alpine:3.14
ENV NODE_VERSION=v16.13.2 NPM_VERSION=8 YARN_VERSION=v1.22.17 NODE_BUILD_PYTHON=python3 OPENSSL_VERSION=openssl-3.0.0+quic

RUN apk upgrade --no-cache -U && \
  apk add --no-cache bash curl make gcc g++ ${NODE_BUILD_PYTHON} linux-headers binutils-gold gnupg libstdc++ perl git

RUN for key in \
      4ED778F539E3634C779C87C6D7062848A1AB005C \
      94AE36675C464D64BAFA68DD7434390BDBE9B9C5 \
      74F12602B6F1C4E913FAA37AD3A89613643B6201 \
      71DCFD284A79C3B38668286BC97EC7A07EDE3FC1 \
      8FCCA13FEF1D0C2E91008E09770F7A9A5AE15600 \
      C4F0DFFF4E8C1A8236409D08E73BC641CC11F4C8 \
      C82FA3AE1CBEDC6BE46B9360C43CEC45C17AB93C \
      DD8F2338BAE7501E3DD5AC78C273792F7D83545D \
      A48C2BEE680E841632CD4E44F07496B3EB3C1762 \
      108F52B48DB57BB0CC439B2997B01419BD92F80A \
      B9E2F5981AA6E0CD28160D9FF13993A75599653C \
    ; do \
      gpg --batch --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys "$key" || \
      gpg --batch --keyserver hkp://ipv4.pool.sks-keyservers.net --recv-keys "$key" || \
      gpg --batch --keyserver pool.sks-keyservers.net --recv-keys "$key" || \
      gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "$key" || \
      gpg --batch --keyserver hkp://pgp.mit.edu:80 --recv-keys "$key" ; \
  done

WORKDIR /tmp/

ENV OPENSSL_DIR=/opt/openssl-with-fips

RUN git clone https://github.com/quictls/openssl.git /tmp/openssl && \
   cd /tmp/openssl && git checkout ${OPENSSL_VERSION} && \
   ./config --prefix=${OPENSSL_DIR} shared enable-fips linux-x86_64 && \
   make -j$(getconf _NPROCESSORS_ONLN) | tee -a compilation.out | awk 'NR%100==0 {print "Logged " NR " lines into compilation.out"}' && \
   make install_ssldirs && \
   make install_fips  && \
   make install

ENV OPENSSL_CONF=${OPENSSL_DIR}/ssl/openssl.cnf
ENV LD_LIBRARY_PATH=${OPENSSL_DIR}/lib64

ADD openssl.cnf ${OPENSSL_CONF}

RUN curl -sfSLO https://nodejs.org/dist/${NODE_VERSION}/node-${NODE_VERSION}.tar.xz && \
  curl -sfSL https://nodejs.org/dist/${NODE_VERSION}/SHASUMS256.txt.asc | gpg -d -o SHASUMS256.txt && \
  grep " node-${NODE_VERSION}.tar.xz\$" SHASUMS256.txt | sha256sum -c | grep ': OK$' && \
  tar -xf node-${NODE_VERSION}.tar.xz && \
  cd node-${NODE_VERSION} && \
  ./configure --shared-openssl --shared-openssl-libpath=${LD_LIBRARY_PATH} \
   --shared-openssl-includes=${OPENSSL_DIR}/include \
   --shared-openssl-libname=crypto,ssl --openssl-is-fips && \
  make -j$(getconf _NPROCESSORS_ONLN)  | tee -a compilation.out | awk 'NR%100==0 {print "Logged " NR " lines into compilation.out"}' && \
  make install

RUN if [ -n "$NPM_VERSION" ]; then \
      npm install -g npm@${NPM_VERSION}; \
    fi; \
    find /usr/local/lib/node_modules/npm -type d \( -name test -o -name .bin \) | xargs rm -rf; \
    if [ -n "$YARN_VERSION" ]; then \
      npm install -g yarn@${YARN_VERSION}; \
    fi;

WORKDIR /

RUN apk del bash curl make gcc g++ ${NODE_BUILD_PYTHON} linux-headers binutils-gold gnupg perl ${DEL_PKGS} && \
  rm -rf /tmp/* \
    /usr/share/man/* /usr/share/doc /root/.npm /root/.node-gyp /root/.config \
    /usr/local/lib/node_modules/npm/man /usr/local/lib/node_modules/npm/doc /usr/local/lib/node_modules/npm/docs \
    /usr/local/lib/node_modules/npm/html /usr/local/lib/node_modules/npm/scripts \
    /opt/openssl-with-fips/share/man/* /opt/openssl-with-fips/share/doc \ 
    /var/cache/* \
    /usr/local/share/man/*  /usr/local/share/doc && \ 
  { rm -rf /root/.gnupg || true; }

RUN node -p process.versions.openssl && \
  node -p 'process.config.variables.openssl_is_fips' && \
  node --enable-fips -p 'crypto.getFips()' && \
  node -p 'crypto.getFips()'
